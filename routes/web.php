<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('tesoreria.index');
});

Route::get('tesoreria','TesoreriaController@index')->name('tesoreria.index');

Route::get('tesoreria/create','TesoreriaController@create')->name('tesoreria.create');
Route::post('tesoreria/create','TesoreriaController@store')->name('tesoreria.store');
Route::get('tesoreria/edit/{id}', 'TesoreriaController@edit')->name('tesoreria.edit');
Route::put('tesoreria/update/{id}' , 'TesoreriaController@update')->name('tesoreria.update');
Route::delete('tesoreria/{id}' , 'TesoreriaController@destroy')->name('tesoreria.destroy');
Route::get('tesoreria/show/{id}' , 'TesoreriaController@show')->name('tesoreria.show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout','Auth\LoginController@logout')->name('logout');
